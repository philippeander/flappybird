package com.project.flappybird;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

//import java.awt.Color;
import java.util.Random;

public class FlappyBird extends ApplicationAdapter {

    private enum GameState{
        wait,
        playing,
        gameOver
    }

	//Configurations Attibute (General)
	private SpriteBatch batch;
    private ShapeRenderer shapeRenderer;
    private GameState gameState;
	private float WidthDeviceScreen;
	private float HeightDeviceScreen;
	private float deltaTime;
	private Random rdm;

	//Bird Attibute
	private Texture[] bird_tex;
	private float bird_ImageFrame = 0;
	private float bird_frameRate = 7.5f;
	private float bird_GravityVelocity = 0;
	private float bird_ImpuseForse = -10;
	private float bird_Position_Y = 0;
    private float bird_Position_X = 122;
	private float bird_minPosition_Y = 0;
	private Circle bird_collider;

	//Scenary Attibute
	private Texture backGround_tex;
	private Texture tube_up_tex;
	private Texture tube_Down_tex;
	private Rectangle tube_up_collider;
	private Rectangle tube_down_collider;
	private float tube_position_X = 0;
	private float tube_spaceBetweenThem;
	private float tube_RandomLenghtBetweenThem;
	private float tube_Speed = 200f;

	//Pop-up Attribute
    private int pontuation = 0;
    private boolean isScoredPoint = false;

    private BitmapFont points_fonte;
    private float points_font_scale = 6;
    private float points_font_size = 50f;

    private Texture gameOver_tex;
    private BitmapFont message_font;
    private float message_font_scale = 3;
    private float  message_font_size = 30f;

    //Camera Attribute
    private OrthographicCamera camera;
    private Viewport viewport;

    private static final String GAME_OVER_MESSAGE = "Tap to Reload Game!";
    private static final float VIRTUAL_HEIGHT = 1024f;
    private static final float VIRTUAL_WIDTH = 768f;

	@Override
	public void create () {

		//bird initialize
		bird_tex = new Texture[4];
		bird_tex[0] = new Texture("passaro1.png");
		bird_tex[1] = new Texture("passaro2.png");
		bird_tex[2] = new Texture("passaro3.png");
		bird_tex[3] = new Texture("passaro4.png");
		bird_collider = new Circle();

		//Scenary  Initialize
		backGround_tex = new Texture("fundo.png");

		tube_up_tex = new Texture("cano_topo.png");
		tube_Down_tex = new Texture("cano_baixo.png");

		//Pop-up Initialize
        gameOver_tex = new Texture("game_over.png");
        message_font = new BitmapFont();

        //Camera Initialize
        camera = new OrthographicCamera();
        camera.position.set(VIRTUAL_WIDTH/2, VIRTUAL_HEIGHT/2, 0);
        viewport = new StretchViewport(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, camera);

		//Config Initialize
		batch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();
		gameState = GameState.wait;
		rdm = new Random();
		points_fonte = new BitmapFont();

		//WidthDeviceScreen = Gdx.graphics.getWidth();
		//HeightDeviceScreen = Gdx.graphics.getHeight();
        WidthDeviceScreen = VIRTUAL_WIDTH;
        HeightDeviceScreen = VIRTUAL_HEIGHT;

		bird_Position_Y = HeightDeviceScreen / 2;

		tube_position_X = WidthDeviceScreen;
		tube_spaceBetweenThem = 300;

		points_fonte.setColor(Color.WHITE);
		points_fonte.getData().setScale(points_font_scale);

        message_font.setColor(Color.WHITE);
        message_font.getData().setScale(message_font_scale);
	}

	@Override
	public void render () {
		//Gdx.gl.glClearColor(1, 0, 0, 1);
		//Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        //move++;

        camera.update();
        //Clears previous frames
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        deltaTime = Gdx.graphics.getDeltaTime();

        //Check the bird animation loop
        bird_ImageFrame += deltaTime * bird_frameRate;
        if (bird_ImageFrame > (bird_tex.length - 1)) bird_ImageFrame = 0;

        if(gameState == GameState.wait){
            if(Gdx.input.justTouched()){
                gameState = GameState.playing;
            }
        }else {

            bird_GravityVelocity++;
            //Make bird Physics
            if (bird_Position_Y > bird_minPosition_Y || bird_GravityVelocity < bird_minPosition_Y) {
                bird_Position_Y -= bird_GravityVelocity;
            }

            if(gameState == GameState.playing){

                //Make tube Moviment
                tube_position_X -= deltaTime * tube_Speed;

                // Get Input Touch
                if (Gdx.input.justTouched()) {
                    bird_GravityVelocity = bird_ImpuseForse;
                }

                //check the horizontal position of the tube
                if (tube_position_X < -tube_up_tex.getWidth()) {
                    tube_position_X = WidthDeviceScreen;
                    tube_RandomLenghtBetweenThem = rdm.nextInt(400) - 200;
                    isScoredPoint = false;

                }

                //Pontuation
                if(tube_position_X < bird_Position_X){
                    if(!isScoredPoint){
                        pontuation++;
                        isScoredPoint = true;
                    }
                }

            }else{
                if(Gdx.input.justTouched()){
                    gameState = GameState.wait;
                    pontuation = 0;
                    bird_GravityVelocity = 0;
                    bird_Position_Y = HeightDeviceScreen/2;
                    tube_position_X = WidthDeviceScreen;

                }
            }

        }

        //Configure Camera Data Projection
        batch.setProjectionMatrix(camera.combined);

		batch.begin();
		batch.draw(backGround_tex, 0, 0, WidthDeviceScreen, HeightDeviceScreen);
		batch.draw(tube_up_tex, tube_position_X, ((HeightDeviceScreen /2) + (tube_spaceBetweenThem /2)) + tube_RandomLenghtBetweenThem);
		batch.draw(tube_Down_tex, tube_position_X, (((HeightDeviceScreen /2) - tube_Down_tex.getHeight()) - (tube_spaceBetweenThem /2)) + tube_RandomLenghtBetweenThem);
		batch.draw(bird_tex[(int) bird_ImageFrame], bird_Position_X, bird_Position_Y);
		points_fonte.draw(batch, String.valueOf(pontuation), WidthDeviceScreen/2, HeightDeviceScreen - points_font_size);

		if(gameState == GameState.gameOver){
		    batch.draw(gameOver_tex, (WidthDeviceScreen/2) - (gameOver_tex.getWidth()/2), HeightDeviceScreen/2);
		    message_font.draw(batch, GAME_OVER_MESSAGE, (WidthDeviceScreen/2)-(200), (HeightDeviceScreen/2)-(gameOver_tex.getHeight()/2));
        }

		batch.end();


		bird_collider.set(bird_Position_X + (bird_tex[0].getWidth()/2),
                          bird_Position_Y + (bird_tex[0].getWidth()/2),
                          bird_tex[0].getWidth()/2);
		tube_down_collider = new Rectangle(
		        tube_position_X, (((HeightDeviceScreen /2) - tube_Down_tex.getHeight()) - (tube_spaceBetweenThem /2)) + tube_RandomLenghtBetweenThem,
                tube_Down_tex.getWidth(), tube_Down_tex.getHeight()
        );

        tube_up_collider = new Rectangle(
                tube_position_X, ((HeightDeviceScreen /2) + (tube_spaceBetweenThem /2)) + tube_RandomLenghtBetweenThem,
                tube_up_tex.getWidth(), tube_up_tex.getHeight()
        );
/*
		//DEBUG: Draw forms
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.circle(bird_collider.x, bird_collider.y, bird_collider.radius);
        shapeRenderer.rect(tube_down_collider.x, tube_down_collider.y, tube_down_collider.width, tube_down_collider.height);
        shapeRenderer.rect(tube_up_collider.x, tube_up_collider.y, tube_up_collider.width, tube_up_collider.height);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.end();
*/

        //Collider test
        if(Intersector.overlaps(bird_collider, tube_up_collider) || Intersector.overlaps(bird_collider, tube_down_collider) ||
                                bird_Position_Y <= 0 || bird_Position_Y >= HeightDeviceScreen){
            gameState = GameState.gameOver;
        }

	}
    @Override
    public void dispose () {
        //batch.dispose();
        //bird.dispose();
    }

    @Override
    public void resize(int width, int height) {
        //super.resize(width, height);
        viewport.update(width, height);
    }
}
